#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <sstream>
#include "Helper.h"
#include <windows.h>
#include <fstream>

using namespace std;;

typedef int(__stdcall *f_funci)();

vector<string> dman;
const int bufferSize = 2048;
char oldDir[bufferSize]; // store the current directory

void thedir()
{
	// get the current directory, and store it
	if (!GetCurrentDirectoryA(bufferSize, const_cast<LPSTR>(oldDir)))
	{
		std::cerr << "Error getting current directory: #" << GetLastError();
		return; // quit if it failed
	}
	std::cout << "Current directory: " << oldDir << '\n';
	return;
}

void thenewdir(char* newDir)
{
	if (!SetCurrentDirectoryA(const_cast<LPCSTR>(newDir)))
	{
		std::cerr << "Error setting current directory: #" << GetLastError();
		return; // quit if we couldn't set the current directory
	}
	std::cout << "Set current directory to " << newDir << '\n';
}

void thenewfile(string filename)
{
	HANDLE newfile = CreateFileA(const_cast<LPCSTR>(filename.c_str()), GENERIC_READ | GENERIC_WRITE,0,0,0, OPEN_EXISTING,0);
}

void thewhatinthedir()
{
	WIN32_FIND_DATA ffd;
	LARGE_INTEGER filesize;
	TCHAR szDir[MAX_PATH];
	size_t length_of_arg;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	DWORD dwError = 0;
	if (sizeof(oldDir) > (MAX_PATH - 3))
	{
		printf(TEXT("\nDirectory path is too long.\n"));
		return;
	}
	strcpy(szDir,oldDir);
	strcat(szDir,TEXT("\\*"));
	hFind = FindFirstFileA(szDir, &ffd);

	do
	{
		if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			printf(TEXT("  %s   <DIR>\n"), ffd.cFileName);
		}
		else
		{
			filesize.LowPart = ffd.nFileSizeLow;
			filesize.HighPart = ffd.nFileSizeHigh;
			printf(TEXT("  %s   %ld bytes\n"), ffd.cFileName, filesize.QuadPart);
		}
	} while (FindNextFileA(hFind, &ffd) != 0);
	FindClose(hFind);
}

int thesecret()
{
	HINSTANCE hGetProcIDDLL = LoadLibrary("C:\\Users\\magshimim\\Downloads\\SECRET.dll");
	f_funci funci = (f_funci)GetProcAddress(hGetProcIDDLL, "funci");
}

void  theexecutable(char* NAME)
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	char* temp;
	strcat(temp, NAME);
	const_cast<LPCSTR>(temp);
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));
	if (!
		CreateProcessA
		(
			temp,
			NULL, NULL, NULL, FALSE,
			CREATE_NEW_CONSOLE,
			NULL, NULL,
			&si,
			&pi
		)
		)
	{
		cout << "Unable to execute.";
	}
}

int main()
{
	string line;
	GetCurrentDirectoryA(bufferSize, oldDir);
	while (getline(cin, line))
	{
		std::istringstream command(line); 
		std::string cmd;
		command >> cmd;
		dman = Helper::get_words(cmd);
		
		if (dman.front == "pwd") thedir();
		else if (dman.front == "cd") thenewdir(dman.back);
		else if (dman.front == "create") thenewfile(dman.back);
		else if (dman.front == "ls") thewhatinthedir();
		else if (dman.front == "secret") thesecret();
		else if ((dman.front).find(".exe") != string::npos) theexecutable(dman.front);
		else cout << "Invalid Command" << endl;
	}
}